﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Data.SqlClient;
using System.Data;

namespace Teatr2
{
    class Wydarzenie
    {
        
        public static int LiczSpektakle;
        public static Wydarzenie[] eventObject = new Wydarzenie[12];



        private int SpektaklID;
        private string Tytuł;
        private string Opis;
        private Image Obraz;

        public static void zaladujSpektakl()
        {
            SqlDataAdapter dAdapter;
            DataSet dSet;

            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Kleo\Desktop\informatyka\Teatr2\teatr.mdf;Integrated Security=True;Connect Timeout=30");
            con.Open();
            dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = new SqlCommand("Select * FROM Wydarzenie", con);
            dSet = new DataSet("dSet");
            dAdapter.Fill(dSet);
            con.Close();
            DataTable dTable;
            dTable = dSet.Tables[0];

            DataTable dataTable = dSet.Tables[0];
            LiczSpektakle = dataTable.Rows.Count;

            for (int i = 0; i < LiczSpektakle; i++)
            {
                eventObject[i] = new Wydarzenie();
                DataRow dataRow = dataTable.Rows[i];
                string finalString = "pic" + Convert.ToString(i);
                FileStream FS1 = new FileStream(finalString + "jpg", FileMode.Create);
                byte[] blob = (byte[])dataRow[3];
                FS1.Write(blob, 0, blob.Length);
                FS1.Close(); FS1 = null;

                eventObject[i].setObraz(Image.FromFile(finalString + "jpg"));
                eventObject[i].setSpektaklID((int)dataRow[0]);
                eventObject[i].setTytuł(Convert.ToString(dataRow[1]));
                eventObject[i].setOpis(Convert.ToString(dataRow[2]));

            }






    }

        

        public void setSpektaklID(int e)
        {
            SpektaklID = e;
        }
        public int getSpektaklID()
        {
            return SpektaklID;
        }
        public void setTytuł(string t)
        {
            Tytuł = t;
        }
        public string getTytuł()
        {
            return Tytuł;
        }
        public void setOpis(string o)
        {
            Opis = o;
        }
        public string getOpis()
        {
            return Opis;
        }
        public void setObraz(Image im)
        {
            Obraz = im;
        }
        public Image getObraz()
        {
            return Obraz;
        }


        public static void dodajwydarzenie(string im, string t, string o)
        {
            FileStream fs; fs = new FileStream(im, FileMode.Open, FileAccess.Read); byte[] picbyte = new byte[fs.Length]; fs.Read(picbyte, 0, System.Convert.ToInt32(fs.Length)); fs.Close();

            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Kleo\Desktop\informatyka\Teatr2\teatr.mdf;Integrated Security=True;Connect Timeout=30");
            con.Open();
            string query = "INSERT INTO Wydarzenie(tytul,opis,obraz) " + 
                "VALUES('" + t + "','" + o + "'," + " @pic)";
            SqlParameter picparameter = new SqlParameter
            {
                SqlDbType = SqlDbType.Image,
                ParameterName = "pic",
                Value = picbyte
            };
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.Add(picparameter);
            cmd.ExecuteNonQuery();
            con.Close();
        }
    }

}

