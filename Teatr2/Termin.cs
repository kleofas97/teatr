﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Teatr2
{
    class Termin
    {
        private int TerminID;
        private int SztukaID;
        private DateTime DataTermin;
        private string Godzina;
        private double CenaMiejsca;

        public static int LiczenieTerminów;
        public static Termin[] DanyTermin = new Termin[12];

        public static void WczytajTermin(int e)
        {
            LiczenieTerminów = 0;
            DataSet dsPerformances = new DataSet();

            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Kleo\Desktop\informatyka\Teatr2\teatr.mdf;Integrated Security=True;Connect Timeout=30");
            con.Open();
            SqlCommand cmTermin = new SqlCommand();
            cmTermin.Connection = con;
            cmTermin.CommandType = CommandType.Text;
            cmTermin.CommandText = "SELECT * FROM Termin WHERE wydarzenieID='" + e + "'";
            SqlDataAdapter daPerformances = new SqlDataAdapter(cmTermin);
            daPerformances.Fill(dsPerformances);
            con.Close();

            LiczenieTerminów = dsPerformances.Tables[0].Rows.Count;
            for (int i = 0; i < LiczenieTerminów; i++)
            {
                DanyTermin[i] = new Termin();
                DataRow dataRow = dsPerformances.Tables[0].Rows[i];
                DanyTermin[i].setTerminID((int)dataRow[0]);
                DanyTermin[i].setSztukaID((int)dataRow[1]);
                DanyTermin[i].setDataTerminu(Convert.ToDateTime(dataRow[2]));
                DanyTermin[i].setGodzina(Convert.ToString(dataRow[3]));
                DanyTermin[i].setCenaMiejsca(Convert.ToDouble(dataRow[4])); }
        }

        public static void SzczegolyTerminu(int e)
        {

            DataSet dsTermin = new DataSet();

            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Kleo\Desktop\informatyka\Teatr2\teatr.mdf;Integrated Security=True;Connect Timeout=30");
            con.Open();
            SqlCommand cmPerformances = new SqlCommand();
            cmPerformances.Connection = con;
            cmPerformances.CommandType = CommandType.Text;
            cmPerformances.CommandText = "SELECT * FROM Termin WHERE terminID='" + e + "'";
            SqlDataAdapter daPerformances = new SqlDataAdapter(cmPerformances);
            daPerformances.Fill(dsTermin);
            con.Close();

            DanyTermin[0] = new Termin();
            DataRow dataRow = dsTermin.Tables[0].Rows[0];
            DanyTermin[0].setTerminID((int)dataRow[0]);
            DanyTermin[0].setSztukaID((int)dataRow[1]);
            DanyTermin[0].setDataTerminu(Convert.ToDateTime(dataRow[2]));
            DanyTermin[0].setGodzina(Convert.ToString(dataRow[3]));
            DanyTermin[0].setCenaMiejsca(Convert.ToDouble(dataRow[4]));

        }




        public static void DodajTermin(int Sz, DateTime d, string t, double c)
        {
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Kleo\Desktop\informatyka\Teatr2\teatr.mdf;Integrated Security=True;Connect Timeout=30");
            con.Open();
            SqlCommand cmTermin = new SqlCommand();
            cmTermin.Connection = con;
            cmTermin.CommandType = CommandType.Text;
            cmTermin.CommandText = "INSERT INTO Termin(wydarzenieID, dataterminu," + "godzinaterminu, cenamiejsca) VALUES ('" + Sz + "','" + d.ToString("MM/dd/yyyy") + "','" + t + "','" + c + "')"; cmTermin.ExecuteNonQuery();

            cmTermin.CommandText = "SELECT SCOPE_IDENTITY()";
            int ID = Convert.ToInt32(cmTermin.ExecuteScalar());
            con.Close();
            Przypisz_miejsca(ID);
        }
        public static void Przypisz_miejsca(int TerminID)
        {
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Kleo\Desktop\informatyka\Teatr2\teatr.mdf;Integrated Security=True;Connect Timeout=30");
            con.Open();
            SqlCommand cmMiejsce = new SqlCommand();
            cmMiejsce.Connection = con;
            cmMiejsce.CommandType = CommandType.Text;
            for (int rząd = 1; rząd <= 11; rząd++)
            {
                char literarzędu = Convert.ToChar(64 + rząd); //litery klejnych rzędów
               
                    for (int miejsce = 1; miejsce <= 20; miejsce++)
                    {
                    cmMiejsce.CommandText = "INSERT INTO Miejsce(rzad,numer," + "terminID,dostepny,rezerwcjaID) VALUES ('" + literarzędu + "','" + miejsce + "','" + TerminID + "','" + "0" + "','" + "0" + "')";
                    cmMiejsce.ExecuteNonQuery();
                    }
            }
        con.Close();
        }



        public void setTerminID(int TID)
        {
            TerminID = TID;
        }

        public int getTerminID()
        {
            return TerminID;
        }

        public void setSztukaID(int Sz)
        {
            SztukaID = Sz;
        }

        public int getSztukaID()
        {
            return SztukaID;
        }

        public void setDataTerminu(DateTime d)
        {
            DataTermin = d;
        }

        public DateTime getDataTerminu()
        {
            return DataTermin;
        }

        public void setGodzina(string t)
        {
            Godzina = t;
        }

        public string getGodzina()
        {
            return Godzina;
        } 
        public void setCenaMiejsca(double c)
        {
            CenaMiejsca = c;
        }

        public double getCenaMiejsca()
        {
            return CenaMiejsca;
        }



    }
}
