﻿namespace Teatr2
{
    partial class Dodajwydarzenie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_wczytaj = new System.Windows.Forms.Button();
            this.btn_zamknij = new System.Windows.Forms.Button();
            this.btn_zapisz = new System.Windows.Forms.Button();
            this.label_tytuł = new System.Windows.Forms.Label();
            this.label_opis = new System.Windows.Forms.Label();
            this.text_tytuł = new System.Windows.Forms.TextBox();
            this.text_opis = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_wczytaj
            // 
            this.btn_wczytaj.Location = new System.Drawing.Point(433, 51);
            this.btn_wczytaj.Name = "btn_wczytaj";
            this.btn_wczytaj.Size = new System.Drawing.Size(136, 23);
            this.btn_wczytaj.TabIndex = 0;
            this.btn_wczytaj.Text = "Załaduj Plakat";
            this.btn_wczytaj.UseVisualStyleBackColor = true;
            this.btn_wczytaj.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_zamknij
            // 
            this.btn_zamknij.AllowDrop = true;
            this.btn_zamknij.Location = new System.Drawing.Point(588, 417);
            this.btn_zamknij.Name = "btn_zamknij";
            this.btn_zamknij.Size = new System.Drawing.Size(105, 48);
            this.btn_zamknij.TabIndex = 1;
            this.btn_zamknij.Text = "Zamknij";
            this.btn_zamknij.UseVisualStyleBackColor = true;
            this.btn_zamknij.Click += new System.EventHandler(this.btn_zamknij_Click);
            // 
            // btn_zapisz
            // 
            this.btn_zapisz.Location = new System.Drawing.Point(50, 417);
            this.btn_zapisz.Name = "btn_zapisz";
            this.btn_zapisz.Size = new System.Drawing.Size(120, 48);
            this.btn_zapisz.TabIndex = 2;
            this.btn_zapisz.Text = "Zapisz";
            this.btn_zapisz.UseVisualStyleBackColor = true;
            this.btn_zapisz.Click += new System.EventHandler(this.button3_Click);
            // 
            // label_tytuł
            // 
            this.label_tytuł.AutoSize = true;
            this.label_tytuł.Location = new System.Drawing.Point(47, 61);
            this.label_tytuł.Name = "label_tytuł";
            this.label_tytuł.Size = new System.Drawing.Size(32, 13);
            this.label_tytuł.TabIndex = 3;
            this.label_tytuł.Text = "Tytuł";
            // 
            // label_opis
            // 
            this.label_opis.AutoSize = true;
            this.label_opis.Location = new System.Drawing.Point(47, 142);
            this.label_opis.Name = "label_opis";
            this.label_opis.Size = new System.Drawing.Size(60, 13);
            this.label_opis.TabIndex = 4;
            this.label_opis.Text = "Opis Sztuki";
            // 
            // text_tytuł
            // 
            this.text_tytuł.Location = new System.Drawing.Point(50, 97);
            this.text_tytuł.Name = "text_tytuł";
            this.text_tytuł.Size = new System.Drawing.Size(262, 20);
            this.text_tytuł.TabIndex = 5;
            // 
            // text_opis
            // 
            this.text_opis.Location = new System.Drawing.Point(50, 168);
            this.text_opis.Multiline = true;
            this.text_opis.Name = "text_opis";
            this.text_opis.Size = new System.Drawing.Size(262, 209);
            this.text_opis.TabIndex = 6;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(354, 97);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(339, 314);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // Dodajwydarzenie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 579);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.text_opis);
            this.Controls.Add(this.text_tytuł);
            this.Controls.Add(this.label_opis);
            this.Controls.Add(this.label_tytuł);
            this.Controls.Add(this.btn_zapisz);
            this.Controls.Add(this.btn_zamknij);
            this.Controls.Add(this.btn_wczytaj);
            this.Name = "Dodajwydarzenie";
            this.Text = "Dodaj Sztukę";
            this.Load += new System.EventHandler(this.Dodajwydarzenie_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_wczytaj;
        private System.Windows.Forms.Button btn_zamknij;
        private System.Windows.Forms.Button btn_zapisz;
        private System.Windows.Forms.Label label_tytuł;
        private System.Windows.Forms.Label label_opis;
        private System.Windows.Forms.TextBox text_tytuł;
        private System.Windows.Forms.TextBox text_opis;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}