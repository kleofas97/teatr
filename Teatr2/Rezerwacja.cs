﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Teatr2
{
    class Rezerwacja
    {
        private int RezerwacjaID;
        private int KlientID;
        private int TerminID;
        private double Koszt_całkowity;

        public static int DodajRezerwacje(int TerminID, int KlientID, double KC, int[,] StatusMiejsca)
        { SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Kleo\Desktop\informatyka\Teatr2\teatr.mdf;Integrated Security=True;Connect Timeout=30");
            con.Open();
            SqlCommand cmRezerwacja = new SqlCommand();
            cmRezerwacja.Connection = con;
            cmRezerwacja.CommandType = CommandType.Text;
            cmRezerwacja.CommandText = "INSERT INTO Rezerwacja(klientID,terminID,kosztcalkowity)" + "VALUES ('" + KlientID + "','" + TerminID + "','" + KC + "')";
            cmRezerwacja.ExecuteNonQuery();
            cmRezerwacja.CommandText = "SELECT SCOPE_IDENTITY()";
            int identity = Convert.ToInt32(cmRezerwacja.ExecuteScalar());
            con.Close();
            return identity;
        }


        public void setRezerwacjaID(int R)
        {
            RezerwacjaID = R;
        }
        public int getRezerwacjaID()
        {
            return RezerwacjaID;
        }
        public void setKlientID(int k)
        {
            KlientID = k;
        }
        public int getKlientID()
        {
            return KlientID;
        }
        public void setTerminID(int t)
        {
            TerminID = t;
        }
        public int getTerminID()
        {
            return TerminID;
        }
        public void setKoszt_calkowity(double KC)
        {
            Koszt_całkowity = KC;
        }
        public double getKoszt_calkowity()
        {
            return Koszt_całkowity;
        }

       
    }
}
