﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Teatr2
{
    public partial class Dodaj_termin : Form
    {
        public Dodaj_termin()
        {
            InitializeComponent();

            wczytajSpektakle();
            
        }

        private void wczytajSpektakle()
        {
            string tytuł;
            combo.Items.Clear();
            for (int i = 0; i < Wydarzenie.LiczSpektakle; i++)
            {
                tytuł = Wydarzenie.eventObject[i].getTytuł();
                combo.Items.Add(tytuł);
            }
        }

            private void btn_Dodaj_termin_Click(object sender, EventArgs e)
        {
            DateTime dataTerminu = Convert.ToDateTime(data_terminu.Value);
            string GodzinaSpektaklu = text_godzina.Text;
            double Cena = Convert.ToDouble(text_cena.Text);
            int i = combo.SelectedIndex;
            int SpektaklID = Wydarzenie.eventObject[i].getSpektaklID();
            Termin.DodajTermin(SpektaklID, dataTerminu, GodzinaSpektaklu, Cena);
            this.Close();
        }

        private void combo_spektakle_SelectedIndexChanged(object sender, EventArgs e)
        {
            lista_terminów.Items.Clear();
            int i = combo.SelectedIndex;
            int eventID = Wydarzenie.eventObject[i].getSpektaklID();
            try
            {
                Termin.WczytajTermin(eventID);
            } catch
            {
                MessageBox.Show("Błąd");
            }
            for (i = 0; i < Termin.LiczenieTerminów; i++)
            {
                DateTime Dataterminu = Termin.DanyTermin[i].getDataTerminu();
                string format = " ddd d MMM yyyy";
                string Dataterminustring = Dataterminu.ToString(format);
                lista_terminów.Items.Add("Data: " + Dataterminustring);
                string CzasSpektaklu = Termin.DanyTermin[i].getGodzina();
                lista_terminów.Items.Add("Godzina: " + CzasSpektaklu);
                double CenaMiejsca = Termin.DanyTermin[i].getCenaMiejsca();
                string CenaMiejscastring = CenaMiejsca.ToString("f2");
                lista_terminów.Items.Add("Cena Miejsca" + CenaMiejscastring);
                lista_terminów.Items.Add(""); }
        }

        private void zamknij_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lista_terminów_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
