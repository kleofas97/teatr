﻿namespace Teatr2
{
    partial class Dane_Klienta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.combo_Spis_Klientow = new System.Windows.Forms.ComboBox();
            this.text_Imie = new System.Windows.Forms.TextBox();
            this.text_Nazwisko = new System.Windows.Forms.TextBox();
            this.text_Ulica = new System.Windows.Forms.TextBox();
            this.text_NrDomu = new System.Windows.Forms.TextBox();
            this.text_KodPocztowy = new System.Windows.Forms.TextBox();
            this.text_Miejscowosc = new System.Windows.Forms.TextBox();
            this.text_mail = new System.Windows.Forms.TextBox();
            this.text_NrTelefonu = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_Zamknij = new System.Windows.Forms.Button();
            this.btnPotwierdzenie = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // combo_Spis_Klientow
            // 
            this.combo_Spis_Klientow.FormattingEnabled = true;
            this.combo_Spis_Klientow.Location = new System.Drawing.Point(207, 29);
            this.combo_Spis_Klientow.Name = "combo_Spis_Klientow";
            this.combo_Spis_Klientow.Size = new System.Drawing.Size(338, 21);
            this.combo_Spis_Klientow.TabIndex = 0;
            this.combo_Spis_Klientow.SelectedIndexChanged += new System.EventHandler(this.combo_Spis_Klientow_SelectedIndexChanged);
            // 
            // text_Imie
            // 
            this.text_Imie.Location = new System.Drawing.Point(207, 80);
            this.text_Imie.Name = "text_Imie";
            this.text_Imie.Size = new System.Drawing.Size(186, 20);
            this.text_Imie.TabIndex = 1;
            // 
            // text_Nazwisko
            // 
            this.text_Nazwisko.Location = new System.Drawing.Point(207, 118);
            this.text_Nazwisko.Name = "text_Nazwisko";
            this.text_Nazwisko.Size = new System.Drawing.Size(186, 20);
            this.text_Nazwisko.TabIndex = 2;
            // 
            // text_Ulica
            // 
            this.text_Ulica.Location = new System.Drawing.Point(207, 209);
            this.text_Ulica.Name = "text_Ulica";
            this.text_Ulica.Size = new System.Drawing.Size(244, 20);
            this.text_Ulica.TabIndex = 3;
            // 
            // text_NrDomu
            // 
            this.text_NrDomu.Location = new System.Drawing.Point(207, 255);
            this.text_NrDomu.Name = "text_NrDomu";
            this.text_NrDomu.Size = new System.Drawing.Size(39, 20);
            this.text_NrDomu.TabIndex = 4;
            // 
            // text_KodPocztowy
            // 
            this.text_KodPocztowy.Location = new System.Drawing.Point(207, 330);
            this.text_KodPocztowy.Name = "text_KodPocztowy";
            this.text_KodPocztowy.Size = new System.Drawing.Size(100, 20);
            this.text_KodPocztowy.TabIndex = 5;
            // 
            // text_Miejscowosc
            // 
            this.text_Miejscowosc.Location = new System.Drawing.Point(207, 295);
            this.text_Miejscowosc.Name = "text_Miejscowosc";
            this.text_Miejscowosc.Size = new System.Drawing.Size(167, 20);
            this.text_Miejscowosc.TabIndex = 6;
            // 
            // text_mail
            // 
            this.text_mail.Location = new System.Drawing.Point(207, 447);
            this.text_mail.Name = "text_mail";
            this.text_mail.Size = new System.Drawing.Size(244, 20);
            this.text_mail.TabIndex = 7;
            // 
            // text_NrTelefonu
            // 
            this.text_NrTelefonu.Location = new System.Drawing.Point(207, 485);
            this.text_NrTelefonu.Name = "text_NrTelefonu";
            this.text_NrTelefonu.Size = new System.Drawing.Size(100, 20);
            this.text_NrTelefonu.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(80, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Wybierz Klienta";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(72, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Imię";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(72, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Nazwisko";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(69, 212);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Ulica";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(72, 258);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Nr Domu";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(72, 298);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Miejscowość";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(72, 333);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Kod Pocztowy";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(69, 450);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "E-mail";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(69, 488);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Numer telefonu";
            // 
            // btn_Zamknij
            // 
            this.btn_Zamknij.Location = new System.Drawing.Point(542, 485);
            this.btn_Zamknij.Name = "btn_Zamknij";
            this.btn_Zamknij.Size = new System.Drawing.Size(129, 44);
            this.btn_Zamknij.TabIndex = 18;
            this.btn_Zamknij.Text = "Anuluj";
            this.btn_Zamknij.UseVisualStyleBackColor = true;
            this.btn_Zamknij.Click += new System.EventHandler(this.btn_Zamknij_Click);
            // 
            // btnPotwierdzenie
            // 
            this.btnPotwierdzenie.Location = new System.Drawing.Point(542, 330);
            this.btnPotwierdzenie.Name = "btnPotwierdzenie";
            this.btnPotwierdzenie.Size = new System.Drawing.Size(129, 59);
            this.btnPotwierdzenie.TabIndex = 19;
            this.btnPotwierdzenie.Text = "Potwierdź";
            this.btnPotwierdzenie.UseVisualStyleBackColor = true;
            this.btnPotwierdzenie.Click += new System.EventHandler(this.btnPotwierdzenie_Click);
            // 
            // Dane_Klienta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 573);
            this.Controls.Add(this.btnPotwierdzenie);
            this.Controls.Add(this.btn_Zamknij);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.text_NrTelefonu);
            this.Controls.Add(this.text_mail);
            this.Controls.Add(this.text_Miejscowosc);
            this.Controls.Add(this.text_KodPocztowy);
            this.Controls.Add(this.text_NrDomu);
            this.Controls.Add(this.text_Ulica);
            this.Controls.Add(this.text_Nazwisko);
            this.Controls.Add(this.text_Imie);
            this.Controls.Add(this.combo_Spis_Klientow);
            this.Name = "Dane_Klienta";
            this.Text = "Dane_Klienta";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox combo_Spis_Klientow;
        private System.Windows.Forms.TextBox text_Imie;
        private System.Windows.Forms.TextBox text_Nazwisko;
        private System.Windows.Forms.TextBox text_Ulica;
        private System.Windows.Forms.TextBox text_NrDomu;
        private System.Windows.Forms.TextBox text_KodPocztowy;
        private System.Windows.Forms.TextBox text_Miejscowosc;
        private System.Windows.Forms.TextBox text_mail;
        private System.Windows.Forms.TextBox text_NrTelefonu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btn_Zamknij;
        private System.Windows.Forms.Button btnPotwierdzenie;
    }
}