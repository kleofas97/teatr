﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Teatr2
{
    class Miejsce
    {
        private char MiejsceRzad;
        private int MiejsceNumer;
        private int TerminID;
        private int dostepne;
        private int rezerwacjaID;

        public static int liczenieMiejsc;
        public static Miejsce[] DaneMiejsce = new Miejsce[240];

        public static void AktualizujMiejscaPoRezerwacji(int TerminID, char Rzad, int NumerMiejsca, int RezerwacjaID)
        {
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Kleo\Desktop\informatyka\Teatr2\teatr.mdf;Integrated Security=True;Connect Timeout=30");
            con.Open(); SqlCommand cmMiejsca = new SqlCommand();
            cmMiejsca.Connection = con;
            cmMiejsca.CommandType = CommandType.Text;
            cmMiejsca.CommandText = "UPDATE Miejsce SET dostepny='1', rezerwcjaID='" + RezerwacjaID + "' WHERE terminID='" + TerminID + "' AND rzad='" + Rzad + "' AND numer='" + NumerMiejsca + "'";
            cmMiejsca.ExecuteNonQuery();
            con.Close();
        }





        public static void LadujMiejsca(int T) // int terminID
        {
            liczenieMiejsc = 0;
            DataSet dsMiejsca = new DataSet();
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Kleo\Desktop\informatyka\Teatr2\teatr.mdf;Integrated Security=True;Connect Timeout=30");
            con.Open();
            SqlCommand cmMiejsca = new SqlCommand();
            cmMiejsca.Connection = con;
            cmMiejsca.CommandType = CommandType.Text;
            cmMiejsca.CommandText = "SELECT * FROM Miejsce WHERE terminID='" + T + "'";
            SqlDataAdapter daMiejsca = new SqlDataAdapter(cmMiejsca);
            daMiejsca.Fill(dsMiejsca);
            con.Close();

            liczenieMiejsc = dsMiejsca.Tables[0].Rows.Count;
            for (int i = 0; i < liczenieMiejsc; i++)
            {
                DaneMiejsce[i] = new Miejsce();
                DataRow dataRow = dsMiejsca.Tables[0].Rows[i];
                DaneMiejsce[i].setMiejsceRzad(Convert.ToChar(dataRow[0]));
                DaneMiejsce[i].setMiejsceNumer((int)dataRow[1]);
                DaneMiejsce[i].setTerminID((int)dataRow[2]);
                DaneMiejsce[i].setdostepne((int)dataRow[3]);
                DaneMiejsce[i].setrezerwacjaID((int)dataRow[4]);
            }
        }

        public void setMiejsceRzad(char r)
        {
            MiejsceRzad = r;
        }
        public char getMiejsceRzad()
        {
            return MiejsceRzad;
        }
        public void setMiejsceNumer(int n)
        {
            MiejsceNumer = n;
        }
        public int getMiejsceNumer()
        {
            return MiejsceNumer;
        }
        public void setTerminID(int T)
        {
            TerminID = T;
        }
        public int getTerminID()
        {
            return TerminID;
        }
        public void setdostepne(int d)
        {
            dostepne = d;
        }
        public int getdostepne()
        {
            return dostepne;
        }

        public void setrezerwacjaID(int r)
        {
            rezerwacjaID = r;
        }

        public int getrezerwacjaID()
        {
            return rezerwacjaID;
        }
    }
}
