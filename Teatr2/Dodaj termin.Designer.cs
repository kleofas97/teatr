﻿namespace Teatr2
{
    partial class Dodaj_termin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_Dodaj_termin = new System.Windows.Forms.Button();
            this.zamknij = new System.Windows.Forms.Button();
            this.text_cena = new System.Windows.Forms.TextBox();
            this.text_godzina = new System.Windows.Forms.TextBox();
            this.data_terminu = new System.Windows.Forms.DateTimePicker();
            this.lista_terminów = new System.Windows.Forms.ListBox();
            this.combo = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Spektakl";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(48, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Terminy";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(60, 310);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Data";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(44, 355);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Godzina";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(57, 414);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Cena ";
            // 
            // btn_Dodaj_termin
            // 
            this.btn_Dodaj_termin.Location = new System.Drawing.Point(63, 465);
            this.btn_Dodaj_termin.Name = "btn_Dodaj_termin";
            this.btn_Dodaj_termin.Size = new System.Drawing.Size(128, 43);
            this.btn_Dodaj_termin.TabIndex = 5;
            this.btn_Dodaj_termin.Text = "Dodaj Termin";
            this.btn_Dodaj_termin.UseVisualStyleBackColor = true;
            this.btn_Dodaj_termin.Click += new System.EventHandler(this.btn_Dodaj_termin_Click);
            // 
            // zamknij
            // 
            this.zamknij.Location = new System.Drawing.Point(323, 465);
            this.zamknij.Name = "zamknij";
            this.zamknij.Size = new System.Drawing.Size(118, 43);
            this.zamknij.TabIndex = 6;
            this.zamknij.Text = "Zamknij";
            this.zamknij.UseVisualStyleBackColor = true;
            this.zamknij.Click += new System.EventHandler(this.zamknij_Click);
            // 
            // text_cena
            // 
            this.text_cena.Location = new System.Drawing.Point(184, 407);
            this.text_cena.Name = "text_cena";
            this.text_cena.Size = new System.Drawing.Size(100, 20);
            this.text_cena.TabIndex = 7;
            // 
            // text_godzina
            // 
            this.text_godzina.Location = new System.Drawing.Point(184, 355);
            this.text_godzina.Name = "text_godzina";
            this.text_godzina.Size = new System.Drawing.Size(129, 20);
            this.text_godzina.TabIndex = 8;
            // 
            // data_terminu
            // 
            this.data_terminu.Location = new System.Drawing.Point(184, 310);
            this.data_terminu.Name = "data_terminu";
            this.data_terminu.Size = new System.Drawing.Size(200, 20);
            this.data_terminu.TabIndex = 9;
            // 
            // lista_terminów
            // 
            this.lista_terminów.FormattingEnabled = true;
            this.lista_terminów.Location = new System.Drawing.Point(184, 120);
            this.lista_terminów.Name = "lista_terminów";
            this.lista_terminów.Size = new System.Drawing.Size(200, 160);
            this.lista_terminów.TabIndex = 10;
            this.lista_terminów.SelectedIndexChanged += new System.EventHandler(this.lista_terminów_SelectedIndexChanged);
            // 
            // combo
            // 
            this.combo.FormattingEnabled = true;
            this.combo.Location = new System.Drawing.Point(184, 41);
            this.combo.Name = "combo";
            this.combo.Size = new System.Drawing.Size(200, 21);
            this.combo.TabIndex = 11;
            this.combo.SelectedIndexChanged += new System.EventHandler(this.combo_spektakle_SelectedIndexChanged);
            // 
            // Dodaj_termin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 559);
            this.Controls.Add(this.combo);
            this.Controls.Add(this.lista_terminów);
            this.Controls.Add(this.data_terminu);
            this.Controls.Add(this.text_godzina);
            this.Controls.Add(this.text_cena);
            this.Controls.Add(this.zamknij);
            this.Controls.Add(this.btn_Dodaj_termin);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Dodaj_termin";
            this.Text = "Dodaj_termin";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_Dodaj_termin;
        private System.Windows.Forms.Button zamknij;
        private System.Windows.Forms.TextBox text_cena;
        private System.Windows.Forms.TextBox text_godzina;
        private System.Windows.Forms.DateTimePicker data_terminu;
        private System.Windows.Forms.ListBox lista_terminów;
        private System.Windows.Forms.ComboBox combo;
    }
}