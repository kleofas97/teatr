﻿namespace Teatr2
{
    partial class Potwierdzenie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.listBox_szcz_rezerwacji = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_Zamknij = new System.Windows.Forms.Button();
            this.btnRezerwuj = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(95, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Szczególy zamówienia";
            // 
            // listBox_szcz_rezerwacji
            // 
            this.listBox_szcz_rezerwacji.FormattingEnabled = true;
            this.listBox_szcz_rezerwacji.Location = new System.Drawing.Point(98, 67);
            this.listBox_szcz_rezerwacji.Name = "listBox_szcz_rezerwacji";
            this.listBox_szcz_rezerwacji.Size = new System.Drawing.Size(400, 277);
            this.listBox_szcz_rezerwacji.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(98, 363);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(218, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Aby potwierdzić zamówienie, kliknij Rezerwuj";
            // 
            // btn_Zamknij
            // 
            this.btn_Zamknij.Location = new System.Drawing.Point(402, 406);
            this.btn_Zamknij.Name = "btn_Zamknij";
            this.btn_Zamknij.Size = new System.Drawing.Size(95, 44);
            this.btn_Zamknij.TabIndex = 3;
            this.btn_Zamknij.Text = "Zamknij";
            this.btn_Zamknij.UseVisualStyleBackColor = true;
            this.btn_Zamknij.Click += new System.EventHandler(this.btn_Zamknij_Click);
            // 
            // btnRezerwuj
            // 
            this.btnRezerwuj.Location = new System.Drawing.Point(101, 406);
            this.btnRezerwuj.Name = "btnRezerwuj";
            this.btnRezerwuj.Size = new System.Drawing.Size(171, 44);
            this.btnRezerwuj.TabIndex = 4;
            this.btnRezerwuj.Text = "Rezerwuj";
            this.btnRezerwuj.UseVisualStyleBackColor = true;
            this.btnRezerwuj.Click += new System.EventHandler(this.btnRezerwuj_Click);
            // 
            // Potwierdzenie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 494);
            this.Controls.Add(this.btnRezerwuj);
            this.Controls.Add(this.btn_Zamknij);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listBox_szcz_rezerwacji);
            this.Controls.Add(this.label1);
            this.Name = "Potwierdzenie";
            this.Text = "Potwierdzenie";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBox_szcz_rezerwacji;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_Zamknij;
        private System.Windows.Forms.Button btnRezerwuj;
    }
}