﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Teatr2
{
    public partial class Plan_Sali : Form
    {
        Button[,] btnSiedzenie = new Button[22, 12];

        Label[] Rzad = new Label[12];

        int[,] StatusMiejsca = new int[22,12];


        public Plan_Sali()
        {
            InitializeComponent();
        }
        string SztukaTytuł;
        public void PokażSale(int i)
        {
            SztukaTytuł = Wydarzenie.eventObject[i].getTytuł();
            label_tytul.Text = SztukaTytuł;

            int SztukaID = Wydarzenie.eventObject[i].getSpektaklID();
            Termin.WczytajTermin(SztukaID);
            combo_Termin.Items.Clear();                    
            for (int p = 0; p < Termin.LiczenieTerminów; p++)
            {
                DateTime dataSpektaklu = Termin.DanyTermin[p].getDataTerminu();

                string format = " ddd d MMM yyyy";
                string DataTerminuString = dataSpektaklu.ToString(format);
                string TerminGodzina = Termin.DanyTermin[p].getGodzina();
                combo_Termin.Items.Add(DataTerminuString + " o " + TerminGodzina); }

        }

        private void pokazplan()
        { int MiejscaMAx;
            for (int j = 1; j <=8; j++)
            { MiejscaMAx = 19;
               
              
                for (int i = 1; i <= MiejscaMAx; i++)
                {
                    btnSiedzenie[i, j] = new Button();
                    btnSiedzenie[i, j].Width = 30;
                    btnSiedzenie[i, j].Height = 30;
                    btnSiedzenie[i, j].Left = (30 * i);
                    if(i>10)
                    {
                        btnSiedzenie[i, j].Left += 30;
                    }
                    btnSiedzenie[i, j].Top = (35 * j);
                    btnSiedzenie[i, j].Image = Image.FromFile("C:/Users/Kleo/Desktop/informatyka/Teatr2/Teatr2/Teatr2/siedzenie_biale.png");

                    if(StatusMiejsca[i,j] == 1)
                    {
                        btnSiedzenie[i,j].Image = Image.FromFile("C:/Users/Kleo/Desktop/informatyka/Teatr2/Teatr2/Teatr2/siedzenie_czerwone.png");
                    }
                    if (StatusMiejsca[i, j] == 3)//przed wlasciwa rezerwacja swieca sie na zolto(jakby ktos wszedl do bazy miedzy zaznaczeniem a potwierdzeniem rezerwacji)
                    {
                        btnSiedzenie[i, j].Image = Image.FromFile("C:/Users/Kleo/Desktop/informatyka/Teatr2/Teatr2/Teatr2/siedzenie_zolte.png");
                    }
                    Rzad[j] = new Label();
                Rzad[j].Size = new System.Drawing.Size(24, 30);
                panel1.Controls.Add(Rzad[j]);
                char c = Convert.ToChar(64 + j);
                
                    
                Rzad[j].Text = Convert.ToString(c);
                Rzad[j].Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                Rzad[j].Location = new System.Drawing.Point(335, 10 + 35 * j);

                String Przycisk = "btn";
                    if (j <= 9)
                    {
                        Przycisk += " ";
                    }
                    Przycisk += j;

                    if (i <= 9)
                    {
                        Przycisk += " ";
                    }
                    Przycisk += i;
                
                btnSiedzenie[i, j].Name = Przycisk;
                btnSiedzenie[i, j].Click += new EventHandler(Klikniecie_Miejsca);
                panel1.Controls.Add(btnSiedzenie[i, j]);
            }
        }
        }
        private void Klikniecie_Miejsca(object sender, EventArgs e)
        {
            Button clickedButton = (Button)sender;

            string s = clickedButton.Name;
            int j = Convert.ToInt16(s.Substring(3, 2));
            int i = Convert.ToInt16(s.Substring(5, 2));

            if (StatusMiejsca[i, j] != 1 && StatusMiejsca[i, j] != 3)
            {
                if (StatusMiejsca[i, j] == 0)
                { StatusMiejsca[i, j] = 2;
                    btnSiedzenie[i, j].Image = Image.FromFile("C:/Users/Kleo/Desktop/informatyka/Teatr2/Teatr2/Teatr2/siedzenie_zielone.png"); // zaznaczone do rezerwacji Status =2, Status 1=> zarezerwowane
                }
                else
                {
                    
                    StatusMiejsca[i, j] = 0;
                    btnSiedzenie[i, j].Image = Image.FromFile("C:/Users/Kleo/Desktop/informatyka/Teatr2/Teatr2/Teatr2/siedzenie_biale.png"); } } //Status = 0 => niezarezerwowane
            ZaznaczoneMiejsca();
        }

        double SumaCenyBiletow;

        private void ZaznaczoneMiejsca()
        {
            try
            { int p = combo_Termin.SelectedIndex;
                double CenaBiletu = Termin.DanyTermin[p].getCenaMiejsca();
                SumaCenyBiletow = 0;
                listBox1.Items.Clear();
                for (int j = 1; j <= 11; j++)
                {
                    for (int i = 1; i <= 20; i++)
                    {
                        if (StatusMiejsca[i, j] == 2)
                        { char c = Convert.ToChar(64 + j);
                            if (j > 8) c = Convert.ToChar(65 + j);
                            listBox1.Items.Add("Rząd " + c + "  Miejsce " + i);
                            SumaCenyBiletow +=CenaBiletu;
                        }
                    }
                }
               CenaKoncowa.Text = SumaCenyBiletow.ToString("f2");
            } catch
            {
                MessageBox.Show("Wybierz termin Spektaklu");
            }
        }





        private void btn_Zamknij_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void combo_Termin_SelectedIndexChanged(object sender, EventArgs e)
        {
            int T = combo_Termin.SelectedIndex;
            int TerminID = Termin.DanyTermin[T].getTerminID();
            Miejsce.LadujMiejsca(TerminID);

            for (int i = 0; i < Miejsce.liczenieMiejsc; i++)
            {
                char c = Miejsce.DaneMiejsce[i].getMiejsceRzad();
                int R = ((int)c) - 64;
                if (R > 8) R--;
                int M = Miejsce.DaneMiejsce[i].getMiejsceNumer(); // s=M
                int dostepne = Miejsce.DaneMiejsce[i].getdostepne();
                StatusMiejsca[M, R] = dostepne;
            }

            int totalButtons = panel1.Controls.Count;
            for (int i = 0; i < totalButtons; i++)
            {
                panel1.Controls.RemoveAt(0);
            }
           label5.Text = Termin.DanyTermin[T].getCenaMiejsca().ToString("f2");




            pokazplan();
        }

        private void btn_rezerwuj_Click(object sender, EventArgs e)
        {
            
            Dane_Klienta form_Dane_Klienta = new Dane_Klienta();
            int p = combo_Termin.SelectedIndex;
            int TerminID = Termin.DanyTermin[p].getTerminID();
            RezerwacjaTymczasowa(TerminID, StatusMiejsca);
            form_Dane_Klienta.getRezerwuj(TerminID, SumaCenyBiletow, StatusMiejsca, SztukaTytuł); form_Dane_Klienta.ShowDialog();
            
            this.Close();
        }
        public static void RezerwacjaTymczasowa(int TerminID, int[,] StatusMiejsca)
        {
            int form_TerminID = TerminID;
            for (int j = 1; j <= 11; j++)
            {
                for (int i = 1; i <= 20; i++)
                {
                    if (StatusMiejsca[i, j] == 2)
                    {
                        char c = Convert.ToChar(64 + j);
                        if (j > 8)
                            c = Convert.ToChar(65 + j);
                        RezerwacjaTymczasowa2(form_TerminID,c, i);
                    }
                    
                }
            }


            
        }
        public static  void RezerwacjaTymczasowa2(int TerminID, char c, int i)
        {
            int k = 0;
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Kleo\Desktop\informatyka\Teatr2\teatr.mdf;Integrated Security=True;Connect Timeout=30");
            con.Open();
            SqlCommand cmMiejsca = new SqlCommand();
            cmMiejsca.Connection = con;
            cmMiejsca.CommandType = CommandType.Text;
            cmMiejsca.CommandText = "UPDATE Miejsce SET dostepny='3',rezerwcjaID=' "  + k + "' WHERE terminID='" + TerminID + "' AND rzad='" + c + "' AND numer='" + i + "'";
            cmMiejsca.ExecuteNonQuery();
            con.Close();
        }

        public static void AnulujRezerwacjaTymczasowa(int TerminID, int[,] StatusMiejsca)
        {
            int form_TerminID = TerminID;
            for (int j = 1; j <= 11; j++)
            {
                for (int i = 1; i <= 20; i++)
                {
                    if (StatusMiejsca[i, j] == 2)
                    {
                        char c = Convert.ToChar(64 + j);
                        if (j > 8)
                            c = Convert.ToChar(65 + j);
                        AnulujRezerwacjaTymczasowa2(form_TerminID, c, i);
                    }

                }
            }



        }
        public static void AnulujRezerwacjaTymczasowa2(int TerminID, char c, int i)
        {
            int k = 0;
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Kleo\Desktop\informatyka\Teatr2\teatr.mdf;Integrated Security=True;Connect Timeout=30");
            con.Open();
            SqlCommand cmSeats = new SqlCommand();
            cmSeats.Connection = con;
            cmSeats.CommandType = CommandType.Text;
            cmSeats.CommandText = "UPDATE Miejsce SET dostepny='0',rezerwcjaID=' " + k + "' WHERE terminID='" + TerminID + "' AND rzad='" + c + "' AND numer='" + i + "'";
            cmSeats.ExecuteNonQuery();
            con.Close();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
