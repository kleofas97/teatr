﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Teatr2
{
    public partial class Potwierdzenie : Form
    {
        int form_TerminID;
        int form_KlientID;
        double form_KosztCalkowity;
        int[,] form_StatusMiejsca;

        public Potwierdzenie()
        {
            InitializeComponent();
        }

        public void getPotwierdzenie(int TerminID, double KosztCalkowity, int[,] StatusMiejsca, int KlientID, string TytułSztuki)
        {
            form_TerminID = TerminID;
            form_KosztCalkowity = KosztCalkowity;
            form_StatusMiejsca = StatusMiejsca;
            form_KlientID = KlientID;

            listBox_szcz_rezerwacji.Items.Clear();
            listBox_szcz_rezerwacji.Items.Add("Sztuka: " + TytułSztuki);
            listBox_szcz_rezerwacji.Items.Add(" ");

            Termin.SzczegolyTerminu(TerminID);
            DateTime DataTermin = Termin.DanyTermin[0].getDataTerminu();
            string format = " ddd d MMM yyyy";
            string DataTerminString = DataTermin.ToString(format);
            string TerminGodzina = Termin.DanyTermin[0].getGodzina();
            listBox_szcz_rezerwacji.Items.Add("Data : " + DataTerminString + " o " + TerminGodzina);
            string CenaMiejsca = (Termin.DanyTermin[0].getCenaMiejsca().ToString("f2"));
            listBox_szcz_rezerwacji.Items.Add("Cena miejsca: " + CenaMiejsca + " zł");

            listBox_szcz_rezerwacji.Items.Add("");
            listBox_szcz_rezerwacji.Items.Add("Wybrane Miejsca: ");
            for (int j = 1; j <= 11; j++)
            {
                for (int i = 1; i <= 20; i++)
                {
                    if (StatusMiejsca[i, j] == 2)
                    {
                        char c = Convert.ToChar(64 + j);
                        if (j > 8)
                            c = Convert.ToChar(65 + j);
                        listBox_szcz_rezerwacji.Items.Add("Rząd " + c + " Miejsce " + i);
                    }
                }
            }
            listBox_szcz_rezerwacji.Items.Add("");
            listBox_szcz_rezerwacji.Items.Add("Suma  " + KosztCalkowity.ToString("f2") + " zł");
            listBox_szcz_rezerwacji.Items.Add(" ");

            Klient.SzczegolyKlientow(KlientID);
            string KlientNazwa = Klient.Klientvektor[0].getNazwisko() + " " + Klient.Klientvektor[0].getImie();
            listBox_szcz_rezerwacji.Items.Add(KlientNazwa);
        }

       
        private void btn_Zamknij_Click(object sender, EventArgs e)
        {
            Plan_Sali.AnulujRezerwacjaTymczasowa(form_TerminID, form_StatusMiejsca);
            this.Close();
        }

        private void btnRezerwuj_Click(object sender, EventArgs e)
        {
            char Rzad;
            int NumerMiejsca;
            int NumerRzędu;

            try
            {
                int RezerwacjaID = Rezerwacja.DodajRezerwacje(form_TerminID, form_KlientID, form_KosztCalkowity, form_StatusMiejsca);
                Miejsce.LadujMiejsca(form_TerminID);
                for (int i = 0; i < Miejsce.liczenieMiejsc; i++)
                {
                    Rzad = Miejsce.DaneMiejsce[i].getMiejsceRzad();
                    NumerMiejsca = Miejsce.DaneMiejsce[i].getMiejsceNumer();

                    NumerRzędu = (int)Rzad - 64;
                    if (NumerRzędu > 8)
                        NumerRzędu--;

                    if (form_StatusMiejsca[NumerMiejsca, NumerRzędu] == 2)
                    {
                        Miejsce.AktualizujMiejscaPoRezerwacji(form_TerminID, Rzad, NumerMiejsca, RezerwacjaID);
                    }
                }

                MessageBox.Show("Rezerwacja Dodana");
                this.Close();
            }
            catch
            {
                MessageBox.Show("Błąd");
            }
        }
    }
}
