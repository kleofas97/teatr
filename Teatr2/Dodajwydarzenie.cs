﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Teatr2
{
    public partial class Dodajwydarzenie : Form
    {

        string imagename;
        public Dodajwydarzenie()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FileDialog fileDialog = new OpenFileDialog(); fileDialog.Filter = "Typ rozszerzenia (*.jpg;*.bmp;*.gif)|*.jpg;*.bmp;*.gif";

                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    imagename = fileDialog.FileName;
                    Bitmap newimg = new Bitmap(imagename);
                    pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                    pictureBox1.Image = (Image)newimg;
                }
                fileDialog = null;
            }
            catch { MessageBox.Show("Błąd ładowania obrazu"); }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dodajSztukę();
        }

        private void dodajSztukę()
        {
            try
            {
                text_tytuł.Text = text_tytuł.Text.Replace("'", "`");
                text_opis.Text = text_opis.Text.Replace("'", "`");
                if (imagename != "")
                {
                    MessageBox.Show("wchodze do bazy");
                    Wydarzenie.dodajwydarzenie(imagename, text_tytuł.Text, text_opis.Text);
                    MessageBox.Show("Wydarzenie Dodane");
                }
            }
            catch
            {
                MessageBox.Show("Błąd");

            }
        }

        private void btn_zamknij_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Dodajwydarzenie_Load(object sender, EventArgs e)
        {

        }
    }
}
