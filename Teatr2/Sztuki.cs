﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Teatr2
{
    public partial class Wyświetl : Form
    {
        public Wyświetl()
        {
            InitializeComponent();

            Wydarzenie.LiczSpektakle = 0;
            try
                {
                Wydarzenie.zaladujSpektakl();
                }
            catch(Exception ex)
                {
                MessageBox.Show(ex.Message);
                }
            Wyświetl_Plakaty();
        }
        private void Wyświetl_Plakaty()
        {
            PictureBox[] Obraz = new PictureBox[8];
            Label[] Tytuł = new Label[8];
            TextBox[] Opis = new TextBox[8];
            Button[] rezerwuj = new Button[8];

            for (int i = 0; i < Wydarzenie.LiczSpektakle; i++)
            {
                Obraz[i] = new PictureBox();
                Obraz[i].Image = Wydarzenie.eventObject[i].getObraz();
                Obraz[i].Size = new System.Drawing.Size(600, 400);
                Obraz[i].Location = new System.Drawing.Point(60, 60 + 650 * i);
                Obraz[i].SizeMode = PictureBoxSizeMode.StretchImage;
                panel1.Controls.Add(Obraz[i]);

                Obraz[i].Refresh();

                Tytuł[i] = new Label();
                Tytuł[i].Size = new System.Drawing.Size(800, 50);
                Tytuł[i].Text = Wydarzenie.eventObject[i].getTytuł();
                Tytuł[i].Font = new System.Drawing.Font("Times New Roman", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                Tytuł[i].Location = new System.Drawing.Point(680, 60 + 650 * i);
                panel1.Controls.Add(Tytuł[i]);

                Opis[i] = new TextBox();
                Opis[i].TabStop = false;
                Opis[i].BorderStyle = System.Windows.Forms.BorderStyle.None;
                Opis[i].Location = new System.Drawing.Point(680, 140 + 650 * i);
                Opis[i].Multiline = true; Opis[i].Size = new System.Drawing.Size(500, 200);
                Opis[i].Text = Wydarzenie.eventObject[i].getOpis();
                Opis[i].ReadOnly = true;
                Opis[i].Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                panel1.Controls.Add(Opis[i]);
                rezerwuj[i] = new Button();
                rezerwuj[i].Location = new System.Drawing.Point(1200, 160 + 650 * i);
                rezerwuj[i].Size = new System.Drawing.Size(112, 28);
                rezerwuj[i].Text = "Rezerwuj miejsca";
                String buttonName = "rezerwuj" + i;
                rezerwuj[i].Name = buttonName;
                rezerwuj[i].Click += new EventHandler(Wczytaj_Plan);
                panel1.Controls.Add(rezerwuj[i]); 

            }
        }

        private void Wczytaj_Plan(object sender, EventArgs e)
        {
            Button clickedButton = (Button)sender;
            string s = clickedButton.Name;
            int i = Convert.ToInt16(s.Substring(8, 1));
            Plan_Sali formPlanSali = new Plan_Sali();
            formPlanSali.PokażSale(i);
            formPlanSali.ShowDialog();
        }

        private void Wyświetl_Load(object sender, EventArgs e)
        {

        }

        private void dodajSztukęToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Dodajwydarzenie form_dodajwydarzenie = new Dodajwydarzenie();
            form_dodajwydarzenie.ShowDialog();
        }

        private void wyjścieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dodajTerminToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Dodaj_termin form_dodaj_Termin = new Dodaj_termin();
            form_dodaj_Termin.ShowDialog();
        }
    }
}
