﻿namespace Teatr2
{
    partial class Plan_Sali
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_tytul = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.CenaKoncowa = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_rezerwuj = new System.Windows.Forms.Button();
            this.btn_Zamknij = new System.Windows.Forms.Button();
            this.combo_Termin = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label_tytul
            // 
            this.label_tytul.AutoSize = true;
            this.label_tytul.Location = new System.Drawing.Point(397, 9);
            this.label_tytul.Name = "label_tytul";
            this.label_tytul.Size = new System.Drawing.Size(0, 13);
            this.label_tytul.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 624);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Zajęte miejsca";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(447, 570);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Cena miejsca";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(439, 608);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Cena końcowa: ";
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(100, 141);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(600, 414);
            this.panel1.TabIndex = 4;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(125, 624);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(120, 95);
            this.listBox1.TabIndex = 5;
            // 
            // CenaKoncowa
            // 
            this.CenaKoncowa.Location = new System.Drawing.Point(545, 608);
            this.CenaKoncowa.Name = "CenaKoncowa";
            this.CenaKoncowa.Size = new System.Drawing.Size(100, 20);
            this.CenaKoncowa.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(542, 570);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 13);
            this.label5.TabIndex = 7;
            // 
            // btn_rezerwuj
            // 
            this.btn_rezerwuj.Location = new System.Drawing.Point(570, 696);
            this.btn_rezerwuj.Name = "btn_rezerwuj";
            this.btn_rezerwuj.Size = new System.Drawing.Size(75, 23);
            this.btn_rezerwuj.TabIndex = 8;
            this.btn_rezerwuj.Text = "Rezerwuj";
            this.btn_rezerwuj.UseVisualStyleBackColor = true;
            this.btn_rezerwuj.Click += new System.EventHandler(this.btn_rezerwuj_Click);
            // 
            // btn_Zamknij
            // 
            this.btn_Zamknij.Location = new System.Drawing.Point(400, 696);
            this.btn_Zamknij.Name = "btn_Zamknij";
            this.btn_Zamknij.Size = new System.Drawing.Size(75, 23);
            this.btn_Zamknij.TabIndex = 9;
            this.btn_Zamknij.Text = "Anuluj";
            this.btn_Zamknij.UseVisualStyleBackColor = true;
            this.btn_Zamknij.Click += new System.EventHandler(this.btn_Zamknij_Click);
            // 
            // combo_Termin
            // 
            this.combo_Termin.FormattingEnabled = true;
            this.combo_Termin.Location = new System.Drawing.Point(369, 37);
            this.combo_Termin.Name = "combo_Termin";
            this.combo_Termin.Size = new System.Drawing.Size(208, 21);
            this.combo_Termin.TabIndex = 11;
            this.combo_Termin.SelectedIndexChanged += new System.EventHandler(this.combo_Termin_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(282, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Termin";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label6.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.label6.Font = new System.Drawing.Font("Corbel", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label6.Location = new System.Drawing.Point(100, 91);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(600, 47);
            this.label6.TabIndex = 13;
            this.label6.Text = "Scena";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Plan_Sali
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 761);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.combo_Termin);
            this.Controls.Add(this.btn_Zamknij);
            this.Controls.Add(this.btn_rezerwuj);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.CenaKoncowa);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label_tytul);
            this.Name = "Plan_Sali";
            this.Text = "Plan_Sali";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_tytul;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.TextBox CenaKoncowa;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_rezerwuj;
        private System.Windows.Forms.Button btn_Zamknij;
        private System.Windows.Forms.ComboBox combo_Termin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
    }
}