﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Teatr2
{
    class Klient
    {
        private int KlientID;
        private string Imie;
        private string Nazwisko;
        private string adres;
        private string NrDomu;
        private string Miejscowosc;
        private string Kod_Pocztowy;
        private string email;
        private string Nr_Telefonu;


        public static int liczbaKlientow;
        public static Klient[] 
        Klientvektor = new Klient[12];

        public static void wczytajKlientow()
        {
           liczbaKlientow = 0;
            DataSet dsCustomers = new DataSet(); SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Kleo\Desktop\informatyka\Teatr2\teatr.mdf;Integrated Security=True;Connect Timeout=30");
            con.Open();
            SqlCommand cmCustomers = new SqlCommand();
           cmCustomers.Connection = con;
            cmCustomers.CommandType = CommandType.Text;
            cmCustomers.CommandText = "SELECT * FROM Klient ORDER BY nazwisko ASC";
            SqlDataAdapter daCustomers = new SqlDataAdapter(cmCustomers);
            daCustomers.Fill(dsCustomers);
            con.Close();

            liczbaKlientow = dsCustomers.Tables[0].Rows.Count;
            for (int i = 0; i < liczbaKlientow; i++)
            {
                Klientvektor[i] = new Klient();
                DataRow dataRow = dsCustomers.Tables[0].Rows[i];
                Klientvektor[i].setKlientID((int)dataRow[0]);
                Klientvektor[i].setImie(Convert.ToString(dataRow[1]));
                Klientvektor[i].setNazwisko(Convert.ToString(dataRow[2]));               
                Klientvektor[i].setAdres(Convert.ToString(dataRow[3]));
                Klientvektor[i].setNrDomu(Convert.ToString(dataRow[4]));
                Klientvektor[i].setMiejscowosc(Convert.ToString(dataRow[5]));
                Klientvektor[i].setKod_Pocztowy(Convert.ToString(dataRow[6]));
                Klientvektor[i].setEmail(Convert.ToString(dataRow[7]));
                Klientvektor[i].setNr_Telefonu(Convert.ToString(dataRow[8])); }
        }




        public void setKlientID(int k)
        {
            KlientID = k;
        }
        public int getKlientID()
        {
            return KlientID;
        }
        public void setImie(string I)
        {
            Imie = I;
        }
        public string getImie()
        {
            return Imie;
        }
        public void setNazwisko(string N)
        {
            Nazwisko = N;
        }
        public string getNazwisko()
        {
            return Nazwisko;
        }

        public void setAdres(string a)
        {
            adres = a;
        }
        public string getAdres()
        {
            return adres;
        }
        public void setNrDomu(string a2)
        {
            NrDomu = a2;
        }
        public string getNrDomu()
        {
            return NrDomu;
        }
        public void setMiejscowosc(string M)
        {
            Miejscowosc = M;
        }
        public string getMiejscowosc()
        {
            return Miejscowosc;
        }
        public void setKod_Pocztowy(string KP)
        {
            Kod_Pocztowy = KP;
        }
        public string getKod_Pocztowy()
        {
            return Kod_Pocztowy;
        }
        public void setEmail(string em)
        {
            email = em;
        }
        public string getEmail()
        {
            return email;
        }
        public void setNr_Telefonu(string NrT)
        {
            Nr_Telefonu = NrT;
        }
        public string getNr_Telefonu()
        {
            return Nr_Telefonu;
        }

        public static int ZapiszKlienta(string Imie, string Nazwisko, string Ulica, string NrDomu, string Miejscowosc, string Kod_Pocztowy, string Email, string Nr_Telefonu)
        {
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Kleo\Desktop\informatyka\Teatr2\teatr.mdf;Integrated Security=True;Connect Timeout=30");
            con.Open();
            SqlCommand cmCustomer = new SqlCommand();
            cmCustomer.Connection = con;
            cmCustomer.CommandType = CommandType.Text;
            cmCustomer.CommandText = "INSERT INTO Klient(imie,nazwisko," + "ulica, nrdomu, miejscowosc, kodpocztowy, email, nrtelefonu)" + "VALUES ('" + Imie + "','" + Nazwisko + "','" + Ulica + "','" + NrDomu + "','" + Miejscowosc + "','" + Kod_Pocztowy + "','" + Email + "','" + Nr_Telefonu + "')";
            cmCustomer.ExecuteNonQuery();

            cmCustomer.CommandText = "SELECT SCOPE_IDENTITY()";
            int identity = Convert.ToInt32(cmCustomer.ExecuteScalar());
            con.Close();
            return identity;
        }

        public static void SzczegolyKlientow(int K)
        {
           
            DataSet dsCustomers = new DataSet(); SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Kleo\Desktop\informatyka\Teatr2\teatr.mdf;Integrated Security=True;Connect Timeout=30");
            con.Open();
            SqlCommand cmCustomers = new SqlCommand();
            cmCustomers.Connection = con;
            cmCustomers.CommandType = CommandType.Text;
            cmCustomers.CommandText = "SELECT * FROM Klient" + " WHERE klientID ='" + K + "'";
            SqlDataAdapter daCustomers = new SqlDataAdapter(cmCustomers);
            daCustomers.Fill(dsCustomers);
            con.Close();

            liczbaKlientow = dsCustomers.Tables[0].Rows.Count;
            
                Klientvektor[0] = new Klient();
                DataRow dataRow = dsCustomers.Tables[0].Rows[0];
                Klientvektor[0].setKlientID((int)dataRow[0]);
                Klientvektor[0].setImie(Convert.ToString(dataRow[1]));
                Klientvektor[0].setNazwisko(Convert.ToString(dataRow[2]));
               
            
        }



    }
}


