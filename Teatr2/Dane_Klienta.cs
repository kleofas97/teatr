﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;




namespace Teatr2
{
    public partial class Dane_Klienta : Form
    {
        int form_TerminID;
        double form_KosztCalkowity;
        int[,] form_StatusMiejsca;
        string form_TytułSztuki;

        bool NowyKlient = true;

        public Dane_Klienta()
        {
            InitializeComponent();

            wczytajKlientow();
        }
        private void wczytajKlientow()
        {
            Klient.wczytajKlientow();
            combo_Spis_Klientow.Items.Clear();
            for (int i = 0; i < Klient.liczbaKlientow; i++)
            {
                string NazwaKlienta = Klient.Klientvektor[i].getNazwisko() + "  " + Klient.Klientvektor[i].getImie();
               combo_Spis_Klientow.Items.Add(NazwaKlienta); }
        }

        public void getRezerwuj(int TerminID, double KosztCalkowity, int[,]StatusMiejsca, string TytułSztuki)
        {
            form_TerminID = TerminID;
            form_KosztCalkowity = KosztCalkowity;
            form_StatusMiejsca = StatusMiejsca;
            form_TytułSztuki = TytułSztuki;
        }

        private void btn_Zamknij_Click(object sender, EventArgs e)
        {

            Plan_Sali.AnulujRezerwacjaTymczasowa(form_TerminID, form_StatusMiejsca);
            this.Close(); // dodac zmiane statusu miejsca
        }

        int KlientID;

        private void btnPotwierdzenie_Click(object sender, EventArgs e)
        {
            KlientID = 0;

            if (NowyKlient)
            {

                if (text_Imie.Text.Length > 0 && text_Nazwisko.Text.Length > 0 && text_Ulica.Text.Length > 0 && text_KodPocztowy.Text.Length > 0 && text_Miejscowosc.Text.Length > 0 && text_NrDomu.Text.Length > 0)
                {

                    KlientID = Klient.ZapiszKlienta(text_Imie.Text, text_Nazwisko.Text, text_Ulica.Text, text_NrDomu.Text, text_Miejscowosc.Text, text_KodPocztowy.Text, text_mail.Text, text_NrTelefonu.Text);
                }
                else
                {
                    MessageBox.Show("Proszę wypełnić wszystkie pola");
                }
            }
            else
            {
                int i = combo_Spis_Klientow.SelectedIndex;
                KlientID = Klient.Klientvektor[i].getKlientID();

            }

            if(KlientID>0)
            {
                Potwierdzenie form_potwierdzenie = new Potwierdzenie();
                form_potwierdzenie.getPotwierdzenie(form_TerminID, form_KosztCalkowity, form_StatusMiejsca, KlientID, form_TytułSztuki);
                form_potwierdzenie.ShowDialog();
                this.Close();

            }


        }

        private void combo_Spis_Klientow_SelectedIndexChanged(object sender, EventArgs e)
        {
            NowyKlient = false;

            int i = combo_Spis_Klientow.SelectedIndex;
            text_Imie.Text = Klient.Klientvektor[i].getImie();
            text_Nazwisko.Text = Klient.Klientvektor[i].getNazwisko();
            text_Ulica.Text = Klient.Klientvektor[i].getAdres();
            text_NrDomu.Text = Klient.Klientvektor[i].getNrDomu();
            text_Miejscowosc.Text = Klient.Klientvektor[i].getMiejscowosc();
            text_KodPocztowy.Text = Klient.Klientvektor[i].getKod_Pocztowy();
            text_mail.Text = Klient.Klientvektor[i].getEmail();
            text_NrTelefonu.Text = Klient.Klientvektor[i].getNr_Telefonu();
        }
    }
}
